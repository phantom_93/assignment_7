﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_7
{
    #region Wolf Class 
    public class Wolf : Animal
    {
        public string Skin { get; set; }
        public Wolf(string name, int age, double weight, double height, Gender gender) : base(name, age, weight, height, gender)
        {

        }

        public override void Hunt()
        {
            Console.WriteLine($"{Name} kills its prey by biting it in the neck area\n");
        }
        public override void Hop(double distanceM)
        {
            Console.WriteLine($"{Name} can hop {distanceM} m away\n");
        }


        public override void MakeNoise(string sound)
        {
            Console.WriteLine($"{Name} makes a sound: {sound}\n");
        }

        public override void Sleep()
        {
            Console.WriteLine($"{Name} has gone to bed. Zzzzzz\n");
        }

        public override void Print()
        {
            Console.WriteLine($"{Name} is a {Gender} and {Age} years old, and weigh {Weight}kg and {Height}feet long and it has the skin of {Skin}.\n");

        }
    }
    #endregion


    #region Sub-Wolf species 

    // 4 different types of wolves 
    public class GrayWolf : Wolf
    {
        public GrayWolf(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }

    public class ArcticWolf : Wolf
    {
        public ArcticWolf(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }

    public class EasternWolf : Wolf
    {
        public EasternWolf(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }

    public class IndianWolf : Wolf
    {
        public IndianWolf(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }

    #endregion

}
