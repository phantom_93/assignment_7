﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_7
{
    #region Class Lion

    public class Lion : Animal
    {
        public string Skin { get; set; }
        public Lion(string name, int age, double weight, double height, Gender gender) : base(name, age, weight, height, gender)
        {

        }

        public override void Hunt()
        {
            Console.WriteLine($"{Name} kills their victim by breaking its neck or suffocating it by clamping their jaws around its throat\n");
        }
        public override void Hop(double distanceM)
        {
            Console.WriteLine($"{Name} can hop {distanceM} m away\n");
        }


        public override void MakeNoise(string sound)
        {
            Console.WriteLine($"{Name} makes a sound: {sound}\n");
        }

        public override void Sleep()
        {
            Console.WriteLine($"{Name} has gone to bed. Zzzzzz\n");
        }

        public override void Print()
        {
            Console.WriteLine($"{Name} is a {Gender} and {Age} years old, and weigh {Weight}kg and {Height}feet long and it has the skin of {Skin}.\n");

        }
    }
    #endregion

    #region subclass Lion
    // 4 different types of lions 
    public class WhiteLion : Lion
    {
        public WhiteLion(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }


    public class KatangaLion : Lion
    {
        public KatangaLion(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }



    public class BarbaryLion : Lion
    {
        public BarbaryLion(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }
    }


    public class TransvaalLion : Lion
    {
        public TransvaalLion(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Skin = skin;
        }

    }
    #endregion

}
