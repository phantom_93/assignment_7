Assignment 7 - Zoology 2.0
Upgrade the Zoology application

Use inheritance to allow for the creation of at least two Animal subtypes
Demonstrate the use of abstract classes and polymorphism
Hint: Make one of your supertypes abstract 

Weight: Critical